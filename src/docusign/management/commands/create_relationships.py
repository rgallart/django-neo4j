# -*- encoding: utf8 -*-
from django.core.management.base import BaseCommand, CommandError

import random
import datetime

from docusign.models import User, Envelope
from .constants import NUM_ENVELOPES, NUM_RELACIONS, NUM_USERS, RANDOM_NUMBER_OF_USERS

previous_envelopes = []

def create_relationships():
    global previous_envelopes
    envelope = None
    # get a random envelope previously not selected
    while True:
        random_envelope = random.randint(0, NUM_ENVELOPES)
        if random_envelope not in previous_envelopes:
            envelope = Envelope.nodes.get(pk="e_%s" % random_envelope)
            previous_envelopes.append(random_envelope)
            break

    # get a random user and create a CREATE relationship
    random_user = random.randint(0, NUM_USERS)
    user = User.nodes.get(pk='u_%s' % random_user)

    print "CREATING Creator Relationship [user: %s, envelope: %s]" % (user.pk, envelope.pk)
    envelope.created.connect(user)

    # get a random number of users (including the previous)
    random_number_of_users = random.randint(0, RANDOM_NUMBER_OF_USERS - 1)
    users = [user.pk]
    for i in xrange(random_number_of_users):
        while True:
            random_user = random.randint(0, NUM_USERS)
            pk = 'u_%s' % random_user
            if pk not in users:
                users.append(pk)
                break

    for pk in users:
        user = User.nodes.get(pk=pk)
        print "CREATING Sent Relationship [user: %s, envelope: %s]" % (user.pk, envelope.pk)
        envelope.sent.connect(user, {'status': 'signed'})


class Command(BaseCommand):
    help = 'Creates DocuSign Envelopes, Users and relationships'

    def handle(self, *args, **options):
        ini = datetime.datetime.now()

        for i in xrange(NUM_RELACIONS):
            create_relationships()

        fi = datetime.datetime.now()
        self.stdout.write("Temps execucio: %s" % ((fi - ini) / 1000))
