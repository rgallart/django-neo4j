# -*- encoding: utf8 -*-
from django.core.management.base import BaseCommand, CommandError

import random
import datetime

from docusign.models import User, Envelope
from .constants import NUM_ENVELOPES, NUM_RELACIONS, NUM_USERS, RANDOM_NUMBER_OF_USERS


class Command(BaseCommand):
    help = 'Creates DocuSign Envelopes, Users and relationships'

    def handle(self, *args, **options):
        ini = datetime.datetime.now()

        for i, user in enumerate(User.nodes.all()):
            user.update_trust_score()
            if ( i % 100 == 0 ):
                self.stdout.write("%d users with trust score updated" % i)

        fi = datetime.datetime.now()
        self.stdout.write("Temps execucio: %s" % (fi - ini))
