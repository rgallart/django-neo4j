# -*- encoding: utf8 -*-
from django.core.management.base import BaseCommand, CommandError
from docusign.models import User, Envelope
import random
import datetime

def create_envelope(id_envelope):
    print "CREATING Envelope: %s" % id_envelope
    envelope = Envelope(pk="e_%d" % id_envelope, completed=random.choice([True, False]))
    envelope.save()
    return envelope


class Command(BaseCommand):
    help = 'Creates DocuSign Envelopes'

    def handle(self, *args, **options):
        ini = datetime.datetime.now()
        NUM_ENVELOPES = 100
        for i in xrange(NUM_ENVELOPES):
            create_envelope(i)

        fi = datetime.datetime.now()
        self.stdout.write("Temps execucio: %s" % ((fi - ini) / 1000))
