var trust = {

    init:function(){
        //this.sendEnvelope();
        this.loadAllUsers();
        var dd = this.getData();

        this.searchUser(dd);
    },
    populateUser:function(){},
    loadUser:function(uid){
        $( "#idCard").empty();
        var url = 'http://localhost:8000/trust/'+uid;
        $.getJSON( url, function( data ) {
           console.log(data['pk']);
            //$.each( data, function( key, val ) {
            //   // items.push( '<li>' +key +':'+ val + '</li>' );
            //
            //});
            var htm = "<dl><dd><b>User Name</b>: "+ data['name'] + "</dd><dd><b>ID</b>: "+ data['pk'] + "</dd><dd><b>Trust Score</b>: "+ data['normalized_trust_score'] + "/100</dd><dd><b>Transaction Volume</b>: "+ data['number_envelopes_completed'] + "</dd> <dd><b>Absolute Trust Points</b>: "+ data['trust_score'] + "</dd></dl><div id=\"score\"> <p>"+ data['normalized_trust_score'] + "</p> </div><div id=\"loadings\" class=\"spinner-circle spinner-sm\"></div><button  onclick=\"trust.sendEnvi('"+data['pk']+"')\"  type=\"button\" id=\"sendenv\"  class=\"btn btn-main btn-xl\">Send Envelope</button>";
            $( "#idCard").append(htm);
            $( "#idCard").show();

        });
    },



    //sendEnvelope:function(){
    //    $("#sendenv").click(function(e){
    //
    //
    //    });
    //
    //},
    loadAllUsers:function(){
        var url = 'http://localhost:8000/trust/names';
        $.getJSON( url, function( data ) {
            var items = [];
            $.each( data, function( key, val ) {
                items.push( "<li id='" + key + "'><a href=\"javascript:void(0);\" id=" + val['pk'] + ">" + val['name'] + "</a></li>" );
            });


            $( "<ul/>", {
                "class": "users-list",
                html: items.join( "" )
            }).appendTo( "#userInfo" );

            $("ul.users-list li a").click( function(ev){
                trust.loadUser(ev.target.id);
                ev.preventDefault();
            })
        });

    },
    searchUser:function(data){
        $("#users").kendoAutoComplete({
            dataSource: data,
            filter: "startswith",
            placeholder: "Search User...",
            separator: ", "
        });
    },
    fakeTransaction:function(){

    },
    getData:function(){
        var url = 'http://localhost:8000/trust/names';
        var items = [];
        var dat = $.getJSON( url, function( data ) {
            var d =  $.each( data, function( key, val ) {


                items.push( val['name'] );


          //  console.log(items);
            });
            return d;
           // console.log(d);

        })
        return dat;

    },
    sendEnvi:function(uid){
        $( "#idCard").empty();
        $("#loadings").show();
        var url = 'http://localhost:8000/send_envelope/'+uid;
        $.getJSON( url, function( data ) {

            var htm = "<dl><dd><b>User Name</b>: "+ data['name'] + "</dd><dd><b>ID</b>: "+ data['pk'] + "</dd><dd><b>Trust Score</b>: "+ data['normalized_trust_score'] + "/100</dd><dd><b>Transaction Volume</b>: "+ data['number_envelopes_completed'] + "</dd> <dd><b>Absolute Trust Points</b>: "+ data['trust_score'] + "</dd></dl><div id=\"score\"> <p>"+ data['normalized_trust_score'] + "</p> </div><div id=\"loadings\" class=\"spinner-circle spinner-sm\"></div><button  type=\"button\" id=\"sendenv\"  class=\"btn btn-main btn-xl\">Send Envelope</button>";
            $( "#idCard").append(htm);
            $( "#idCard").show();


        });
    }

}
trust.init();