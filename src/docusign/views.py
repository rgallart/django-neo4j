from django.contrib.auth import login, logout
from django.conf import settings
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import Http404, HttpResponse
from django.shortcuts import redirect
from django.views.generic.base import RedirectView, TemplateView

from neomodel import db

import datetime
import json
import random
import time

from .models import User, Envelope


def get_user_data(user_id):
    user = User.nodes.get(pk=user_id)
    data = {
        'pk': user.pk,
        'name': user.name,
        'trust_score': user.trust_score,
        'normalized_trust_score': user.normalized_trust_score,
        'number_envelopes_completed': user.number_envelopes_completed,
    }

    return data


def trust_data(request, user_id):
    data = get_user_data(user_id)
    response = HttpResponse(json.dumps(data), content_type='application/json')
    return response


def trust_names(request):
    data = []
    query = 'match (u:User) return u.trust_score, u.pk, u.name order by u.trust_score desc limit 100'
    results, meta = db.cypher_query(query)

    for r in results:
        data.append({
            'name': r[2],
            'pk': r[1],
        })

    response = HttpResponse(json.dumps(data), content_type='application/json')

    return response


def _create_envelope(user):
    # print "CREATING Envelope: %s" % id_envelope
    now = datetime.datetime.now()
    envelope = Envelope(pk="e_%s" % now.strftime("%Y%m%d%H%M%s%S"), completed=True)
    envelope.save()
    _create_relationships(envelope, user)


def _create_relationships(envelope, user):
    # print "CREATING Creator Relationship [user: %s, envelope: %s]" % (user.pk, envelope.pk)
    envelope.created.connect(user)

    # get a random number of users (including the previous)
    random_number_of_users = random.randint(0, 10 - 1)
    users = [user.pk]
    for i in xrange(random_number_of_users):
        while True:
            try:
                random_user = random.randint(0, 1000-1)
                pk = 'u_%s' % random_user
                if pk not in users:
                    users.append(pk)
                    break
            except:
                pass

    for pk in users:
        try:
            # print "CREATING Sent Relationship [user: %s, envelope: %s]" % (pk, envelope.pk)
            user = User.nodes.get(pk=pk)
            envelope.sent.connect(user, {'status': 'signed'})
        except:
            pass


def send_envelope(request, user_id):
    user = User.nodes.get(pk=user_id)
    _create_envelope(user)
    user.update_trust_score()
    data = get_user_data(user_id)
    response = HttpResponse(json.dumps(data), content_type='application/json')
    return response


class DashboardView(TemplateView):
    template_name = 'docusign/dashboard.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        return self.render_to_response(context)


class TrustProfileView(TemplateView):
    template_name = 'docusign/trust_profile.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        return self.render_to_response(context)
