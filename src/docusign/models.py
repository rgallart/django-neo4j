from neomodel import StructuredNode, StringProperty, IntegerProperty, BooleanProperty
from neomodel import DateTimeProperty, RelationshipTo, RelationshipFrom, StructuredRel
from neomodel import ZeroOrMore, OneOrMore, ZeroOrOne, One
from datetime import datetime
import pytz


class SendRel(StructuredRel):
    since = DateTimeProperty(default=lambda: datetime.now(pytz.utc))
    status = StringProperty(default='unknown')


class User(StructuredNode):
    pk = StringProperty(unique_index=True)
    name = StringProperty(unique_index=False)
    trust_score = IntegerProperty()

    def update_trust_score(self):
        query = """
            match (u:User)-[:CREATED]->(e:Envelope)-[s:SENT]->(u2:User)
            where u.pk = '%s' and s.status='signed'
            return count(u2) AS ABC
            UNION ALL
            match (u:User)-[:CREATED]->(e:Envelope)-[s:SENT]->(u2:User)
            where u.pk = '%s' and s.status='rejected'
            return count(u2) AS ABC
            UNION ALL
            match (u:User)-[:CREATED]->(e:Envelope)-[:SENT]->(u2:User)
            where u.pk='%s' and e.completed=true
            with e, count(u2)*3 AS subtotal
            RETURN sum(subtotal) AS ABC
            UNION ALL
            match (u:User)-[:CREATED]->(e:Envelope)-[:SENT]->(u2:User)
            where u.pk = '%s'
            return count(distinct u2) AS ABC
            """ % (self.pk, self.pk, self.pk, self.pk)
        results, meta = self.cypher(query)
        self.trust_score = (results[0][0] - results[1][0] + results[2][0]) * results[3][0]
        if self.trust_score < 10:
            self.trust_score = 10
        self.save()

    @property
    def normalized_trust_score(self):
        # get max and min
        query = "match (u:User) return max(u.trust_score), min(u.trust_score)"
        results, meta = self.cypher(query)
        _max = results[0][0]
        _min = results[0][1]

        # get trust score
        trust_score = self.trust_score

        # normalize
        normalized = float((trust_score - _min)) / float((_max - _min))

        # return
        return int(round(normalized * 100))

    @property
    def number_envelopes_completed(self):
        query = "match (u:User)-[:CREATED]->(e:Envelope) where u.pk='%s' return count(e)" % self.pk
        results, meta = self.cypher(query)
        return results[0][0]


class Envelope(StructuredNode):
    pk = StringProperty(unique_index=True)
    completed = BooleanProperty()

    created = RelationshipFrom(User, 'CREATED', cardinality=One)
    sent = RelationshipTo('User', 'SENT', cardinality=OneOrMore, model=SendRel)

