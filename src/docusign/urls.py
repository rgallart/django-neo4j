# -*- encoding: utf8 -*-
__author__ = 'ramon.gallart'
from django.conf.urls import *

from .views import DashboardView, TrustProfileView, trust_data, trust_names, send_envelope

urlpatterns = patterns('',
                       url(r'^dashboard/$', DashboardView.as_view(), name='dashboard'),
                       url(r'^trust/$', TrustProfileView.as_view(), name='trust_profile'),
                       url(r'^trust/names/$', trust_names, name='trust_profile_names'),
                       url(r'^trust/(?P<user_id>\w+)/$', trust_data, name='trust_profile_data'),
                       url(r'^send_envelope/(?P<user_id>\w+)/$', send_envelope, name='send_envelope'),
                       )
