# -*- encoding: utf8 -*-
from docusign.models import User, Envelope
import random
import datetime


def create_envelope(id_envelope):
    print "CREATING Envelope: %s" % id_envelope
    envelope = Envelope(pk="w_%d" % id_envelope, completed=random.choice([True, False]))
    envelope.save()
    return envelope


# def create_character(world, id_char):
#     print "CREATING CHARACTER: %s" % id_char
#     caracter = Character(pk="c-%s-%d" % (world.pk, id_char), name="C-%s-%d" % (world.pk, id_char), age=random.randint(5, 100))
#     caracter.save()
#     world.inhabitant.connect(caracter)
#     return caracter


# def create_relationship(a_char, another_char):
#     print "CREATING RELATIONSHIP: %s with %s" % (a_char.pk, another_char.pk)
#     if random.randint(1, 10) < 5:
#         a_char.friends.connect(another_char)
#     else:
#         a_char.enemies.connect(another_char_char)


def main():
    ini = datetime.datetime.now()
    NUM_ENVELOPES = 100
    NUM_HABITANTS = 1000
    NUM_RELACIONS = 50
    envelopes = []
    for i in xrange(NUM_ENVELOPES):
        envelopes.append(create_envelope(i))

    # chars = []
    # for j in xrange(NUM_HABITANTS):
    #     un_mon = envelopes[random.randint(0, NUM_ENVELOPES - 1)]
    #     chars.append(create_character(un_mon, j))

    # for k in xrange(NUM_HABITANTS):
    #     for l in range(NUM_RELACIONS):
    #         create_relationship(chars[k], chars[random.randint(0, NUM_HABITANTS - 1)])
    fi = datetime.datetime.now()

    print "Temps execució: %s" % ((fi - ini) / 1000)


main()
