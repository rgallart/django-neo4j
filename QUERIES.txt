TOTAL LIFETIME DISTINCT SIGNERS
==========================================

match (u:User)-[:CREATED]->(e:Envelope)-[:SENT]->(u2:User)
where u.pk = 'u_1'
return count(distinct u2)


TOTAL USERS WITH SIGNED ENVELOPES
===================================

match (u:User)-[:CREATED]->(e:Envelope)-[s:SENT]->(u2:User)
where u.pk = 'u_1' and s.status='signed'
return count(u2)

match (u:User)-[:CREATED]->(e:Envelope)-[s:SENT]->(u2:User)
where u.pk = 'u_1' and s.status='signed'
return sum(u2.trust_score*0.1)


TOTAL USERS WITH REJECTED ENVELOPES
===================================

match (u:User)-[:CREATED]->(e:Envelope)-[s:SENT]->(u2:User)
where u.pk = 'u_1' and s.status='rejected'
return count(u2)

match (u:User)-[:CREATED]->(e:Envelope)-[s:SENT]->(u2:User)
where u.pk = 'u_1' and s.status='rejected'
return sum(u2.trust_score*0.1)

NUMBER OF COMPLETED ENVELOPES
=============================

match (u:User)-[:CREATED]->(e:Envelope) where u.pk='u_1' and e.completed=true return count(e)


NUMBER OF SIGNERS OF COMPLETED ENVELOPES
========================================

match (u:User)-[:CREATED]->(e:Envelope)-[:SENT]->(u2:User) where u.pk='u_1' and e.completed=true return count(u2)


TOTAL POINTS FOR COMPLETED ENVELOPES
====================================

match (u:User)-[:CREATED]->(e:Envelope)-[:SENT]->(u2:User)
where u.pk='u_1' and e.completed=true
with e, count(u2)*3 AS subtotal
RETURN sum(subtotal)


MAX AND MIN
===========

match (u:User) return max(u.trust_score), min(u.trust_score)


===========================================================

match (u:User)-[:CREATED]->(e:Envelope)-[s:SENT]->(u2:User)
where u.pk = 'u_123' and s.status='signed'
return sum(u2.trust_score*0.1) AS ABC
UNION ALL
match (u:User)-[:CREATED]->(e:Envelope)-[s:SENT]->(u2:User)
where u.pk = 'u_123' and s.status='rejected'
return sum(u2.trust_score*0.1) AS ABC
UNION ALL
match (u:User)-[:CREATED]->(e:Envelope)-[:SENT]->(u2:User)
where u.pk='u_123' and e.completed=true
with e, count(u2)*3 AS subtotal
RETURN sum(subtotal) AS ABC
UNION ALL
match (u:User)-[:CREATED]->(e:Envelope)-[:SENT]->(u2:User)
where u.pk = 'u_123'
return count(distinct u2) AS ABC
